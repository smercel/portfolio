export const navLinks =[
    {
    id : "acceuil",
    titre : "Acceuil"

    },
    {
        id : "presentation",
        titre : "Presentation"
    
    },
    {
        id : "projets",
        titre : "Mes Projets"
    
    },
    {
        id : "contact",
        titre : "Me Contacter"
    
    },
]
export const headerText =[
    {
    id : "nom",
    titre : "Sebastien Mercellus"

    },
    {
        id : "intitule",
        titre : "Mon portfolio"
    
    },
]
export const Presentation =[
    {
    id : "etudiant",
    content : "Etudiant BTS SIO SLAM"

    },
    {
        id : "experience",
        content : " 2 ans expérience dans le domaine de informatique"
    
    },
    {
        id : "projets",
        content : " ** Projets à mon actif"
    
    },
]
export const projets =[
    {
        id : "images",
        titre :"projet images",
        desc: "Traitement d'images (et manipulation de vecteurs 2D)",
        date :" 22/11/2022",
        technologie  : "C++"
    

    },
    {
        id : "foufoot",
        titre :"projet FouFoot",
        desc: "Mission : Realisation d'une base de donnée(bonus : connexion et requête insertion avec php)",
        date :" 22/11/2022",
        technologie  : "SQL,PHP"
    
    },
    {
        id : "projets",
        titre : " ** Projets à mon actif"
    
    },
]



