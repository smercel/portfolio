import { useInView } from 'react-intersection-observer';
import {React,useRef,useState,useEffect} from 'react'
import styles from '../style';




const Header = () => {
  let display = "<"
  let display2 = ">"
  const { ref: myRef, inView: myElementIsVisible } = useInView();
 
 
  return (
    <div ref={myRef} className= {` text-center py-10 ${myElementIsVisible ? "show":"hideRight"}  `}>
    <h1  className={` text-5xl  xs:text-6xl my-10  text-primary lg:text-7xl   `}>   Sebastien Mercellus</h1>
  
    <p className=' text-slate-500 pt-2  lg:text-4xl lg:mt-5  xs:text-slate-900 xs:font-medium xs:text-2xl text-2xl'> Etudiant en 2ème année de Bts SIO Spécialisé dans le developpement {display} <span className='text-primary '>/applications</span> {display2}</p>
  </div>
  )
}

export default Header