import React from 'react'
import { useInView } from 'react-intersection-observer'
import ball from "../assets/GitLab-Logo.wine.svg"
import blob from "../assets/blob.svg"

// state


//comportement


//affichage

const Projets = (props) => {
  const { ref: myRef, inView: myElementIsVisible } = useInView()
  return (
    <div ref={myRef} id="projets" className={`text-center  my-20  flex flex-row items-center  justify-center   ${myElementIsVisible ? "show":"hideRight"}`}>
      <div className= {` flex flex-col w-7/12 lg:w-5/12  shadow-lg rounded-lg  border-2  ${props.direction}   mx-4`}>
      <h2 className=' text-4xl pb-4'>{props.name}</h2>
      <span className=' text-lg  w-min'>{props.tech}</span>
      <span className='text-2xl'>{props.date} </span>
      <h4 className=' text-xl'>{props.desc}</h4>
     <a  className=' border border-slate-300 rounded-lg mt-5 mb-5 ' href="https://gitlab.com/smercel" target="_blank"> <img src={ball} alt="git lab logo" width="100" /></a>
      </div>
      
    </div>
  )
}

export default Projets