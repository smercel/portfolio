import React from 'react'

const Navbar = () => {
  return (
    <nav className=' py-6 mb-12 flex justify-between '>
            <h1 className=' text-xl font-quicksand lg:text-3xl text-slate-900 px-10'>Mon Portfolio</h1>
            <ul className=' hover:text-slate-900'>
              <li className=' cursor-pointer border rounded-lg hover:bg-slate-700 hover:text-white'><a href="#presentation">presentation</a>  </li>
              <li className=' mt-4 border rounded-lg hover:bg-slate-700 hover:text-white max-w-min'> <a href="#projets">projets</a></li>
              <li className=' mt-4 border rounded-lg hover:bg-slate-700 hover:text-white max-w-min'> <a href="#contact">contact</a></li>
            </ul>
          </nav>
  )
}

export default Navbar