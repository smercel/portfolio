import React from 'react'
import { useInView } from 'react-intersection-observer'
import eyes from '../assets/eyes.png'
import tech from '../assets/tech.png'
import arrow from '../assets/arrow.png'


const Presentation = () => {
  const { ref: myRef, inView: myElementIsVisible } = useInView()
  return (
    <div id='presentation'>
        <div className='flex flex-row items-center gap-4 justify-center flex-wrap'>
        <h2 className='text-5xl  pl-10  lg:text-7xl py-10 text-center pr-4'> Envie en savoir plus sur moi ? </h2><img className='flash' src={eyes} alt="eyes"  /> 
        </div>
  
        
        <div className=' w-full pl-10 text-center shadow-lg rounded-md p-10 '>
        <ul className=' text-3xl flex flex-col gap-20 '> 
          <li>2 ans experience dans le domaine de informatique</li>
          <div className='flex items-center flex-wrap-reverse justify-center'>
        <li >Passionné de Developpment  Application (surtout <span className='text-primary'>web</span>) </li>

        <img src={tech} alt="tech master" width="50"/>
        </div>
        <div className='flex flex-col items-center justify-center gap-10'>
        <li> Mon Projet Professionnel</li>
         <img src={arrow} alt="fleche" width="50"/>
        </div>
        <li className='mb-5'>Devenir developpeur web Front end</li> 
        </ul>
       
        
        </div>
    </div>

  )
}

export default Presentation