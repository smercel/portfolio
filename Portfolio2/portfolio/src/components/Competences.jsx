import React from 'react'
import { useInView } from 'react-intersection-observer';
const Competences = (Props) => {
    const { ref: myRef, inView: myElementIsVisible } = useInView();
  return (
    <div className='py-10 pl-2'>
    <span className='   '> {Props.name}</span>
     <div ref={myRef} className={` h-3 ${Props.size}   ${Props.color} rounded-r-lg ${myElementIsVisible ?"show":"hideRight"} `}></div>
    
    </div>
  )
}

export default Competences