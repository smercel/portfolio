import React from 'react'
import linked from '../assets/linked.png'
import ball from "../assets/GitLab-Logo.wine.svg"
import github from "../assets/github.png"
const Contact = () => {
  return (
    
      
        <div id="contact" className='bg-slate-200 w-full h-50 text-center font-quicksand'>
        <h3 className='text-3xl'> Me Retrouver</h3>
      
      <ul className='mt-5 ml-20 flex gap-10 lg:gap-20 items-center justify-center'>
      <li className=' hover:scale-150'> <a  className='   mt-5' href="https://www.linkedin.com/in/sebastien-mercellus-04898b259/" target="_blank"> <img src={linked} alt="git lab logo" width="100" /></a> </li>
      <li className='hover:scale-150'><a  className='mt-5' href="https://gitlab.com/smercel" target="_blank"> <img src={ball} alt="git lab logo" width="100" /></a></li>
      <li className='hover:scale-150'><a  className='mt-5' href="https://github.com/sweaterDev" target="_blank"> <img src={github} alt="git lab logo" width="90" /></a></li>
      </ul>
        <span> sebastien.mercellus@gmail.com</span>
        <span className='block'>© sebastien mercellus </span>
        
        </div>
      
      
  )
}

export default Contact