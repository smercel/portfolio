import React from 'react';
import {Navbar,Header,Presentation,Projets,Competences, Contact} from './components';
import rocket from './assets/rocket.png'


const App = () => {
  
  return (
    <div>
      <main className='  font-quicksand tracking-wide'>
        <section className=' min-h-screen bg-navimg bg-cover bg-fixed shadow-md'>
          <Navbar />
          <Header/>
        </section>
        <section >
          <Presentation/>
          <h2 className='text-5xl text-center py-10 lg:text-5xl border-t-2 border-l-slate-700'> Mes Competences</h2>
          <h3 className='pl-10 text-2xl'> Frontend</h3>
          <Competences name="html" size=" w-8/12 "color="bg-red-500"/>
          <Competences name ="css" size="w-7/12" color="bg-yellow-500"/>
         
          <Competences name ="javascript" size="w-4/12" color="bg-teal-500"/>
          <Competences name="react" size="w-3/12"color="bg-blue-300" /> 
          <h3 className='pl-10 text-2xl'> Backend</h3>
          <Competences name="My SQL" size="w-6/12" color="bg-orange-300"/>
          <Competences name="java" size ="w-6/12" color="bg-blue-800"/>
          <div className='flex flex-row items-center flex-wrap justify-center gap-4'>
          <h2 className=' text-5xl text-center pb-5 '>Mes Projets</h2>
          <img src={rocket} alt="rocket" width="90" />
          </div>
          <Projets name="Projet images" date="22/11/2021 " desc="Mission : Traitement d'images (et manipulation de vecteurs 2D)" direction="items-center " tech="c++"/>
          
          <Projets name="FouFoot" date="20/05/2021" desc=" Mission : Realisation d'une base de donnée(bonus : connexion et requête insertion avec php)"direction="items-center " tech="SQL"/>
          
          <Projets name="EasyLine" date="16/10/2022" desc=" Mission :  Realisation d'une architecture backend et front end "direction="items-center " tech="java"/>
          <Projets name="GLPI" date="19/11/2022 " desc="Remonter infomations et gestion de tickets" direction="items-center " tech=" Gestion Parc informatique"/>
        </section>
      </main>
      <footer>
        <Contact/>
      </footer>
    </div>
  )
}

export default App