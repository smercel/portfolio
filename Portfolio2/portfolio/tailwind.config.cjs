/** @type {import('tailwindcss').Config} */

module.exports = {
  content: ["./index.html", "./src/**/*.{js,jsx}"],
  mode: "jit",
  theme: {
    extend: {
      colors: {
        mytext: "#e0e1dd",
        primary: "#77bfa3",
        secondary: "#bfd8bd",
        dimWhite: "rgba(255, 255, 255, 0.7)",
        dimBlue: "rgba(9, 151, 124, 0.1)",
      },
      fontFamily: {
        quicksand : ["Quicksand", "sans-serif"],
        coliac : ["Coldiac","sans-serif"],
        laura : ["laura","sans-serif"],
      
      },
    },
    screens: {
      
      xs: "480px",
      ss: "620px",
      sm: "768px",
      md: "1060px",
      lg: "1200px",
      xl: "1700px",
    },
    backgroundImage: {
      'navimg': "url('assets/Background.jpg')",
      'cimg': "url('assets/C.PNG')",
      'ball': "url('assets/ball.jpg')",
      'deep' : "url(assets/igor.jpg)"
      
    }
  },
  plugins: [],
};

